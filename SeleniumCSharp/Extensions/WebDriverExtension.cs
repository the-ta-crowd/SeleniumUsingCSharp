﻿using System;
using System.Diagnostics;
using OpenQA.Selenium;
using SeleniumCSharp.Base;
using SeleniumCSharp.Config;

namespace SeleniumCSharp.Extensions
{
    public static class WebDriverExtension
    {
        internal static void WaitForPageToLoaded(this IWebDriver driver)
        {
            driver.WaitForCondition(drv =>
            {
                string state = drv.ExecuteJs("return document.readyState").ToString()?.ToLower();
                return state == "complete";
            }, Settings.DefaultWait);
        }

        private static void WaitForCondition<T>(this T obj, Func<T, bool> condition, int timeOut)
        {
            bool Execute(T arg)
            {
                try
                {
                    return condition(arg);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }

            var sw = Stopwatch.StartNew();
            while (sw.ElapsedMilliseconds < timeOut)
            {
                if (Execute(obj))
                {
                    break;
                }
            }
        }

        private static object ExecuteJs(this IWebDriver driver, string script) => ((IJavaScriptExecutor)driver).ExecuteScript(script);

        internal static void SwitchToIFrame(this IWebDriver driver)
        {
            IWebElement iframe = driver.FindElement(By.TagName("iframe"));
            DriverContext.Driver.SwitchTo().Frame(iframe);
        }
    }
}