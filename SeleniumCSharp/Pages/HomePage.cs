﻿using System.Threading;
using OpenQA.Selenium;
using SeleniumCSharp.Base;
using SeleniumCSharp.Config;
using SeleniumCSharp.Extensions;

namespace SeleniumCSharp.Pages
{
    public class HomePage : BasePage
    {
        private IWebElement LinkSignIn => DriverContext.Driver.FindElement(By.CssSelector("a[title='Log in to your customer account']"));

        public LoginPage ClickSignIn()
        {
            DriverContext.Driver.WaitForPageToLoaded();
            Thread.Sleep(5000);
            LinkSignIn.ClickElement();
            return GetInstance<LoginPage>();
        }

        public ProductPage OpenAutomationPracticeSite()
        {
            DriverContext.Driver.Navigate().GoToUrl(Settings.Aut);
            DriverContext.Driver.Manage().Window.Maximize();
            return GetInstance<ProductPage>();
        }
    }
}